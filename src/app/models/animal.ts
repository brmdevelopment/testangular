export class Animal {
   name: string;
   description: string;
   year: string;
   image: string;
   user: string;
}
