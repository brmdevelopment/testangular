import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { GLOBAL } from './global';
import {User} from "../models/user";

const httpOptions = {
  headers: new HttpHeaders({'X-DreamFactory-API-Key': '3b84a3b5e47dbbb62b335d112130d38f9caaa7db5c2b3bd4ea3c9bbc2947c527','Content-Type': 'application/json'}
            )};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersUrl: string;
  constructor (private http: HttpClient) {
    this.usersUrl =  GLOBAL.url;
  }

  addUser (user: User): Observable<User> {
    return this.http.post<User>(this.usersUrl+'/user/session', user, httpOptions).pipe(
      tap((user: User) => this.log(`added User w/ id=${user.email}`)),
      catchError(this.handleError<User>('addUser'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log('UserService: ' + message);
  }
}
