import { Component, DoCheck, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck, OnInit{
  title = 'myappUdemyOne';
  emailContacto: string;
  ngOnInit(){
    this.emailContacto = localStorage.getItem('emailContact');
    //console.log(localStorage.getItem('emailContact'));
  }

  ngDoCheck(){
        this.emailContacto = localStorage.getItem('emailContact');
  //  console.log('Docheck ejecutado');
  }

  borrarEmailLocal(){
    localStorage.removeItem('emailContact');
    this.emailContacto= null;
  }
  vaciarLocalStorge(){
    localStorage.clear();
    this.emailContacto= null;
  }


}
