import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, DoCheck, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-parques',
  templateUrl: './parques.component.html',
  styleUrls: ['./parques.component.css']
})
export class ParquesComponent implements OnChanges, OnInit, OnDestroy {
  @Input() nombre: string;
  public metros: number;
  public vegetacion: string;
  public abierto;
  @Output()  pasameLaBotella = new EventEmitter();

  constructor() {
    this.nombre  ="Hola parque";
    this.metros = 40;
    this.vegetacion = "Hola vegetacion";
    this.abierto = false;
  }

  ngOnChanges(changes: SimpleChanges){
    console.log(changes);
  }
  ngOnInit(){
    console.log('Metodo init');
  }

  ngOnDestroy(){
    console.log('Elimina componente');
  }
  emitirEvento(){
    this.pasameLaBotella.emit({
      'nombre': this.nombre,
      'metros': this.metros,
      'vegetacion': this.vegetacion,
      'abierto': this.abierto
    });
  }



}
