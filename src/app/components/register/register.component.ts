import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { GLOBAL } from '../../service/global';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]
})
export class RegisterComponent implements OnInit {
public title: String;
public user: User;

  constructor(
      private _route: ActivatedRoute,
      private _router: Router,
      private userService: UserService
    ) {
        this.title = 'Registro';
        this.user = {
          "email": "stivenson02@gmail.com",
          "password": "123456",
          "remember_me": 'false'
    }
      }

  ngOnInit() {
    console.log('register.component cargado!!');

  }
  onSubmit(){
    this.userService.addUser(this.user).subscribe(data => {
      console.log(data);
    })
  }

}
