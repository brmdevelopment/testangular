import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../service/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
public title: String;
public user: User;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.title = 'Identificate';
    this.user = new User();
  }

  ngOnInit() {
    console.log('login.component cargado!!');
  }

  onSubmit(){
console.log(this.user);
  }

}
