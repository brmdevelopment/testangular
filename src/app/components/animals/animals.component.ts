import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../service/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css'],
  providers: [UserService]
})
export class AnimalsComponent implements OnInit{

  user: User;
  constructor(
    public userService: UserService,
    private http:HttpClient
  ) {
    this.user = {
      "email": "stivenson02@gmail.com",
      "password": "r",
      "remember_me": 'false'
    }

  }

  ngOnInit(): void {
   let obs = this.http.get('https://10.130.210.15/api/v2/user/session');
   obs.subscribe((response) => console.log(response));
  }

  addUser() {

    this.userService.addUser(this.user).subscribe(
      data => {
        console.log(data);
      }
    );
  }


}
