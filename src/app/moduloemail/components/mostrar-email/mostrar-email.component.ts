import { Component, OnInit, DoCheck } from '@angular/core';

@Component({
  selector: 'app-mostrar-email',
  templateUrl: './mostrar-email.component.html',
  styleUrls: ['./mostrar-email.component.css']
})
export class MostrarEmailComponent implements OnInit, DoCheck {

  emailContacto: string;
  ngOnInit(){
    this.emailContacto = localStorage.getItem('emailContact');
    //console.log(localStorage.getItem('emailContact'));
  }

  ngDoCheck(){
        this.emailContacto = localStorage.getItem('emailContact');
  //  console.log('Docheck ejecutado');
  }

  borrarEmailLocal(){
    localStorage.removeItem('emailContact');
    this.emailContacto= null;
  }
  vaciarLocalStorge(){
    localStorage.clear();
    this.emailContacto= null;
  }

}
