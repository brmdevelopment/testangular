import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guardar-email',
  templateUrl: './guardar-email.component.html',
  styleUrls: ['./guardar-email.component.css']
})
export class GuardarEmailComponent implements OnInit {

  emailContacto: string;

  ngOnInit(){
  }

  guardarEmail(){
    localStorage.setItem('emailContact', this.emailContacto);
  }


}
